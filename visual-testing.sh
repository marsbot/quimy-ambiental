#!/usr/bin/env sh
branch_name=$(git symbolic-ref -q HEAD)
branch_name=${branch_name##refs/heads/}
if [ "$branch_name" = "development" ]; then branch_name="stage"; fi

mv backstop.json backstop.json.bck
sed "s/REV/$branch_name.$1/g" backstop.json.bck > backstop.json
docker run --rm -v "$(pwd)":/code registry.gitlab.com/marsbot/docker-images:backstopjs-2.7.5-nodejs-8.1.2-phantomjs-2.1.1-python-3.6.1-slimer-0.10.3-yarn-0.24.6 backstop test
mv backstop.json.bck backstop.json
