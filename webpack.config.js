'use strict'

var webpack = require('webpack')
var path = require('path')

var DEVELOPMENT = Boolean(process.env.BUILD_DEVELOPMENT)
var PRODUCTION = Boolean(process.env.BUILD_PRODUCTION)

var entry = {
  main: './source/js/main.js'
}

var resolve = {
  modules: [
    path.join(__dirname, '/source/js'),
    'node_modules'
  ]
}

var modules = {
  rules: [
    {
      test: /source\/js\/.*\.js$/,
      exclude: /node_modules|\.tmp|vendor/,
      loader: 'babel-loader',
      options: {
        presets: [
          ['env', {
            'debug': DEVELOPMENT,
            'modules': false
          }],
        ],
        cacheDirectory: true
      }
    }
  ]
}

if( DEVELOPMENT ) {
  modules.rules.unshift({
    enforce: 'pre',
    test: /source\/js\/.*.js$/,
    exclude: /node_modules|\.tmp|vendor/,
    loader: 'eslint-loader'
  })
}

var plugins = [
  new webpack.DefinePlugin({
    __DEVELOPMENT__: DEVELOPMENT,
    __PRODUCTION__: PRODUCTION
  })
]

var output = {
  path: path.join(__dirname, '/.tmp/dist'),
  filename: 'js/[name].js',
  sourceMapFilename: 'js/[name].map'
}

module.exports = {
  devtool: DEVELOPMENT ? 'cheap-source-map' : 'hidden-source-map',
  target: 'web',
  entry: entry,
  resolve: resolve,
  module: modules,
  plugins: plugins,
  output: output
}
