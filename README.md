# Quimy Ambiental

## Instructions

To make modifications on this site you neeed to install the project
dependencies using:

```sh
bundle install
yarn install
```

To run the development server use the following command:

```sh
bundle exec middleman
```
