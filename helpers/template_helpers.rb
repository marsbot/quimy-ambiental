module TemplateHelpers
  def page_title
    title = current_page.data.title ? "#{current_page.data.title} -" : "" +
      config[:title]
    title.html_safe
  end

  def title_tag
    "<title>#{page_title}</title>"
  end

  def page_description
    description = "#{current_page.data.description || config[:description]}"
    CGI.escape description
  end

  def current_page_url
    CGI.escape "#{config[:root_url]}#{current_page.url}"
  end

  def page_url page
    CGI.escape "#{config[:root_url]}#{page.url}"
  end

  def page_twitter_card_type
    type = "#{current_page.data.twitter_card_type || 'summary'}"
    type.html_safe
  end

  def page_image
    image = current_page.data.image || config[:site_image]
    image ? image_path(image) : ""
  end

  def twitter_url page=nil
    "https://twitter.com/share?" +
      "text=#{page_title}" +
      "&url=#{page ? page_url(page) : current_page_url}" +
      "&via=#{config[:twitter_handle] ? config[:twitter_handle] : ""}"
  end

  def facebook_url page=nil
    "https://www.facebook.com/sharer/sharer.php?" +
      "u=#{page ? page_url(page) : current_page_url}"
  end

  def google_plus_url page=nil
    "https://plus.google.com/share?" +
      "url=#{page ? page_url(page) : current_page_url}"
  end

  def linkedin_url page=nil
    "http://www.linkedin.com/shareArticle?" +
      "mini=true" +
      "&url=#{page ? page_url(page) : current_page_url}" +
      "&title=#{page_title}" +
      "&summary=#{page_description}" +
      "&source=#{config[:url_root]}"
  end
end
