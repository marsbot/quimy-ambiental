#!/usr/bin/env python3

import argparse
import mimetypes
import os

import boto3

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--delete', action='store_true')
parser.add_argument('--with-subdomain', action='store_true')
args = parser.parse_args()

domain = os.environ.get('DOMAIN')
rev_name = os.environ.get('REV_NAME', '')
endpoint = os.environ.get('ENDPOINT')

if rev_name:
    bucket_name = '%s.%s' % (rev_name, domain)
else:
    bucket_name = domain

site_dir = os.environ.get('SITE_DIR', 'public')
# Make space for the final dir slash (dir/)
site_dir_len = len(site_dir) + 1

s3 = boto3.resource('s3')
client = boto3.client('s3')

if args.with_subdomain:
    import CloudFlare

    cf = CloudFlare.CloudFlare()
    zone_id = cf.zones.get(params={'name': domain})[0]['id']
    subdomain = cf.zones.dns_records.get(zone_id,
                                         params={'name': bucket_name})

    def create_subdomain():
        if not subdomain:
            bucket_url = '%s.%s' % (bucket_name, endpoint)
            cf.zones.dns_records.post(zone_id,
                                    data={'name': bucket_name,
                                            'type': 'CNAME',
                                            'content': bucket_url,
                                            'proxied': True})

    def delete_subdomain():
        subdomain_id = subdomain[0]['id']
        cf.zones.dns_records.delete(zone_id, subdomain_id)

def delete_bucket():
    try:
        bucket = s3.Bucket(bucket_name)
        for page in bucket.objects.pages():
            objects_to_delete = []
            for obj in page:
                objects_to_delete.append({'Key': obj.key})
            bucket.delete_objects(Delete={'Objects': objects_to_delete})
        bucket.delete()
        if args.with_subdomain:
            print('Deleting subdomain: %s' % bucket_name)
            delete_subdomain()
    except client.exceptions.NoSuchBucket:
        pass

def deploy_bucket():
    print("Deploying %s" % bucket_name)

    if not s3.Bucket(bucket_name) in s3.buckets.all():
        client.create_bucket(Bucket=bucket_name)

    response = client.put_bucket_website(
        Bucket=bucket_name,
        WebsiteConfiguration={
            'ErrorDocument': {
                'Key': '404.html'
            },
            'IndexDocument': {
                'Suffix': 'index.html'
            },
        }
    )

    for root, _, filenames in os.walk(site_dir):
        for filename in filenames:
            file = os.path.join(root, filename)
            type = mimetypes.guess_type(file)[0] or ''
            options = {}
            options['ACL'] = 'public-read'
            options['ContentType'] = type
            client.upload_file(
                file,
                bucket_name,
                file[site_dir_len:],
                options
            )
            print('Uploading %s' % file)

    if args.with_subdomain:
        print('Creating subdomain: %s' % bucket_name)
        create_subdomain()
    print('%s deployed' % bucket_name)

if args.delete:
    delete_bucket()
else:
    deploy_bucket()
