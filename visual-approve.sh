#!/usr/bin/env sh
git stash
docker run --rm -v "$(pwd)":/code registry.gitlab.com/marsbot/docker-images:backstopjs-2.7.5-nodejs-8.1.2-phantomjs-2.1.1-python-3.6.1-slimer-0.10.3-yarn-0.24.6 backstop approve
git add test
git commit -m "Approve visual changes"
git stash pop
